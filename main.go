package main

import (
	"database/sql"

	"bitbucket.org/sebavuga/go_crud/config"
	"bitbucket.org/sebavuga/go_crud/groups"
	"bitbucket.org/sebavuga/go_crud/users"
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

func main() {
	var err error
	config.DbCon, err = sql.Open("postgres", "host=db port=5432 user=postgres password=postgres dbname=api sslmode=disable")
	if err != nil {
		panic(err)
	}
	defer config.DbCon.Close()

	router := initRouter()

	groupsGroup := router.Group("/groups")
	{
		groupsGroup.GET("/", groups.GetAll)
		groupsGroup.GET("/:id", groups.GetSingle)
		groupsGroup.POST("/add", groups.Add)
		groupsGroup.POST("/update", groups.Update)
		groupsGroup.POST("/delete/:id", groups.Delete)
	}

	usersGroup := router.Group("/users")
	{
		usersGroup.GET("/", users.GetAll)
		usersGroup.GET("/:id", users.GetSingle)
		usersGroup.POST("/add", users.Add)
		usersGroup.POST("/update", users.Update)
		usersGroup.POST("/delete/:id", users.Delete)
	}

	router.Run(":8080")
}

func initRouter() *gin.Engine {
	router := gin.Default()

	router.GET("/hc", func(c *gin.Context) {
		c.String(200, "As happy as a clam!")
	})

	return router
}
