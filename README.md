# README #

This is a rough implementation of a simple CRUD project.  

### Who is this repository for? ###

* Tim
* Seba (because he wants to work @ Lake)

### Installation ###

* Install Docker
* Use terminal of your choice to move to the root folder
* Run 'docker compose up'

### Usage ###

* Install Postman
* Import Postman collection
* Use imported requests to play around 


### What is missing? ###

* Polish polish polish
* Proper endpoint validation
* Proper unit tests
* Did I mention polish?