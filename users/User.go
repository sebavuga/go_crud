package users

import (
	"net/http"
	"strconv"

	"bitbucket.org/sebavuga/go_crud/config"
	"bitbucket.org/sebavuga/go_crud/jsonNullStructs"
	"github.com/gin-gonic/gin"
)

type User struct {
	ID       int                           `json:"id"`
	GroupID  jsonNullStructs.JsonNullInt64 `json:"group_id"`
	Name     string                        `json:"name" binding:"required"`
	Email    string                        `json:"email" binding:"required"`
	Password string                        `json:"password" binding:"required"`
}

func findById(id int) (response User, ok bool) {
	var user User

	if err := config.DbCon.QueryRow(
		"SELECT * FROM users WHERE id = "+strconv.Itoa(id)).Scan(
		&user.ID,
		&user.GroupID,
		&user.Name,
		&user.Email,
		&user.Password); err != nil {
		return user, false
	}

	return user, true
}

func GetAll(c *gin.Context) {

	rows, err := config.DbCon.Query("SELECT * FROM users")
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	var users []User = []User{}
	for rows.Next() {
		var user User
		if err := rows.Scan(&user.ID, &user.GroupID, &user.Name, &user.Email, &user.Password); err != nil {
			panic(err)
		}

		users = append(users, user)
	}

	c.IndentedJSON(http.StatusOK, users)
}

func Add(c *gin.Context) {
	var newUser User

	if err := c.BindJSON(&newUser); err != nil {
		panic(err)
	}

	var groupId string = "NULL"

	if newUser.GroupID.Valid {
		groupId = strconv.Itoa(int(newUser.GroupID.Int64))
	}

	_, err := config.DbCon.Exec(
		"INSERT INTO users (group_id, name, email, password) VALUES (" +
			groupId + "," +
			"'" + newUser.Name + "'," +
			"'" + newUser.Email + "'," +
			"'" + newUser.Password + "'" +
			")")
	if err != nil {
		panic(err)
	}

	c.String(200, "OK")
}

func Update(c *gin.Context) {
	var newUser User

	if err := c.BindJSON(&newUser); err != nil {
		c.String(400, "Bad Request")
		return
	}

	_, success := findById(newUser.ID)

	if !success {
		c.String(404, "User not found")
		return
	}

	var groupId string = "NULL"

	if newUser.GroupID.Valid {
		groupId = strconv.Itoa(int(newUser.GroupID.Int64))
	}

	_, err := config.DbCon.Exec(
		"UPDATE users SET " +
			"group_id = " + groupId + "," +
			"name = '" + newUser.Name + "'," +
			"email = '" + newUser.Email + "'," +
			"password = '" + newUser.Password + "'" +
			"WHERE id = " + strconv.Itoa(newUser.ID))
	if err != nil {
		panic(err)
	}

	c.String(200, "OK")
}

func GetSingle(c *gin.Context) {
	idString := c.Param("id")
	var id, _ = strconv.Atoi(idString)

	user, success := findById(id)

	if !success {
		c.String(404, "User not found")
		return
	}

	c.IndentedJSON(http.StatusOK, user)
}

func Delete(c *gin.Context) {
	idString := c.Param("id")
	var id, _ = strconv.Atoi(idString)

	_, success := findById(id)

	if !success {
		c.String(404, "User not found")
		return
	}

	_, err := config.DbCon.Exec("DELETE FROM users WHERE id = " + idString)
	if err != nil {
		panic(err)
	}

	c.String(200, "OK")
}
