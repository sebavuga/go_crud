#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER api;
    CREATE DATABASE api;
    GRANT ALL PRIVILEGES ON DATABASE api TO api;
    \connect api api
    BEGIN;
        CREATE TABLE IF NOT EXISTS groups(
            id SERIAL PRIMARY KEY,
            name VARCHAR (50) UNIQUE NOT NULL
        );
        CREATE TABLE users (
            id SERIAL PRIMARY KEY,
            group_id INT,
            name VARCHAR(50) NOT NULL,
            email VARCHAR(50) NOT NULL,
            password VARCHAR(50) NOT NULL,
            FOREIGN KEY (group_id)
                REFERENCES groups (id)
        );
    COMMIT;
EOSQL