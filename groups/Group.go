package groups

import (
	"bitbucket.org/sebavuga/go_crud/config"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type Group struct {
	ID   int    `json:"id"`
	Name string `json:"name" binding:"required"`
}

func findById(id int) (response Group, ok bool) {
	var group Group

	if err := config.DbCon.QueryRow("SELECT * FROM groups WHERE id = "+strconv.Itoa(id)).Scan(&group.ID, &group.Name); err != nil {
		return group, false
	}

	return group, true
}

func Add(c *gin.Context) {
	var newGroup Group

	if err := c.BindJSON(&newGroup); err != nil {
		panic(err)
	}

	_, err := config.DbCon.Exec("INSERT INTO groups (name) VALUES ('" + newGroup.Name + "')")
	if err != nil {
		c.String(409, "A group with this name already exists")
		return
	}

	c.String(200, "OK")
}

func Update(c *gin.Context) {
	var newGroup Group

	if err := c.BindJSON(&newGroup); err != nil {
		c.String(400, "Bad Request")
		return
	}

	_, success := findById(newGroup.ID)

	if !success {
		c.String(404, "Group not found")
		return
	}

	_, err := config.DbCon.Exec("UPDATE groups SET name = '" + newGroup.Name + "' WHERE id = " + strconv.Itoa(newGroup.ID))
	if err != nil {
		c.String(409, "A group with this name already exists")
		return
	}

	c.String(200, "OK")
}

func GetSingle(c *gin.Context) {
	idString := c.Param("id")
	var id, _ = strconv.Atoi(idString)

	group, success := findById(id)

	if !success {
		c.String(404, "Group not found")
		return
	}

	c.IndentedJSON(http.StatusOK, group)
}

func Delete(c *gin.Context) {
	idString := c.Param("id")
	var id, _ = strconv.Atoi(idString)

	_, success := findById(id)

	if !success {
		c.String(404, "Group not found")
		return
	}

	_, err := config.DbCon.Exec("DELETE FROM groups WHERE id = " + idString)
	if err != nil {
		panic(err)
	}

	c.String(200, "OK")
}

func GetAll(c *gin.Context) {

	rows, err := config.DbCon.Query("SELECT * FROM groups")
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	var groups []Group = []Group{}
	for rows.Next() {
		var group Group
		if err := rows.Scan(&group.ID, &group.Name); err != nil {
			panic(err)
		}
		groups = append(groups, group)
	}

	c.IndentedJSON(http.StatusOK, groups)
}
