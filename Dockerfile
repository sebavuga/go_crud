FROM golang:1.17-alpine

LABEL maintainer="Sebastjan Vuga <seba.vuga@gmail.com>"

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY . .

RUN go build -o /go_crud

EXPOSE 8080

CMD [ "/go_crud" ]